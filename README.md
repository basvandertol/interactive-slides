# Interactive Slides

This repository contains a Jupyter notebook that can be viewed as a slide show with interactive code snippets in both Python and C++. The presentation compares operations on arrays using [xtensor](https://github.com/xtensor-stack/xtensor) (C++) and [NumPy](https://www.numpy.org) (Python).
A configuration file for the conda package manager is provided to set up the environment to view the slide show.

## Getting started
[Conda](https://docs.conda.io/en/latest/) is a package and environment manager. Installation instructions (for Linux) can be found [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html).

Once conda is installed an `interactive-slides` environment can be created by from the conda config file [interactive-slides.yml](interactive-slides.yml)
```
conda env create -f interactive-slides.yml
```

## Interactive Slides Environment

Basic packages to make interactive slides:
  - [jupyterlab](https://github.com/jupyterlab/jupyterlab) An extensible environment for interactive and reproducible computing, based on the Jupyter Notebook and Architecture.
  - [sos-notebook](https://github.com/vatlab/SOS)
  - [jupyterlab_sos](https://github.com/vatlab/jupyterlab-sos)
  - [xeus-cling](https://github.com/jupyter-xeus/xeus-cling)
  - [rise](https://github.com/damianavila/RISE) Live Reveal.js Jupyter/IPython Slideshow Extension

  Packages specific for the presentation comparing xtensor and numpy :
  - [numpy](http://numpy.org/) The fundamental package for scientific computing with Python.
  - [matplotlib](http://matplotlib.org/) Matplotlib is a comprehensive library for creating static, animated, and interactive visualizations in Python.
  - [xtensor](https://github.com/xtensor-stack/xtensor) The C++ tensor algebra library
  - [xtensor-blas](https://github.com/xtensor-stack/xtensor-blas) xtensor-blas is an extension to the xtensor library, offering bindings to BLAS and LAPACK libraries through cxxblas and cxxlapack from the FLENS project.
  - [xtensor-fftw](http://github.com/xtensor-stack/xtensor-fftw) FFTW bindings for xtensor
  - [fftw](http://fftw.org) The fastest Fourier transform in the west.


## Launching Jupyter

Locally
```
miniconda3/bin/conda run -n interactive-slides jupyter notebook --no-browser --port 8899
```

Remote
```
ssh hostname -L8899:localhost:8899 -t miniconda3/bin/conda run -n interactive-slides jupyter notebook --no-browser --port 8899
```

## Using multiple kernels

## Cling C++ interpreter

## Creating a slide show

## Launching a side show

## Executing code

